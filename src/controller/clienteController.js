module.exports = class ClienteController {
  // Criar cliente
  static async criarCliente(req, res) {
      const {nome, email, telefone, cpf } = req.body;

      if (nome != "" && telefone != "" && email != 0 && cpf != "")
      {
        
          // Lógica para criar um cliente no banco de dados
          console.log(nome);
          console.log(email);
          console.log(telefone);
          console.log(cpf);
          res.status(200).json({ message: "Cadastrado com sucesso!" });
          
    
      }
      else {
        res.status(400).json({ message: "Dados em Branco" });
      }
  
  }

  // Obter informações do cliente
  static async getCliente(req, res) {
      try {
          // Lógica para obter informações do cliente do banco de dados
          const cliente = {
              nome: 'Gabriel',
              cpf: '12345678910',
              email: 'leo@gmail.com'
          };
          res.status(200).json(cliente);
      } catch (error) {
          console.error("Erro ao obter cliente:", error);
          res.status(500).json({ error: "Erro interno do servidor" });
      }
  }

  // Editar informações do cliente
  static async editCliente(req, res) {
      const { nome, telefone, email, cpf } = req.body;
      try {
          if (nome != "" && telefone != "" && email != 0 && cpf != "") {
              // Lógica para editar o cliente no banco de dados
              res.status(200).json({ message: "Editado com sucesso" });
          } else {
              res.status(400).json({ message: "Dados em Branco" });
          }
      } catch (error) {
          console.error("Erro ao editar cliente:", error);
          res.status(500).json({ error: "Erro interno do servidor" });
      }
  }

  // Excluir cliente
  static async deletarCliente(req, res) {
        if (req.params.id != null){
            res.status(200).json({ message: "Cliente Excluido" });
        }
        else{
            res.status(400).json({ message: "Cliente não encontrado" });
        }
    }
};
