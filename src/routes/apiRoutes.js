const router = require('express').Router();
const LoginController = require('../controller/LoginController');
const clienteController = require("../controller/clienteController");

router.post('/criarCliente/', clienteController.criarCliente);
router.get('/getCliente/', clienteController.getCliente);
router.put('/editCliente/', clienteController.editCliente);
router.delete('/deletCliente/:id', clienteController.deletarCliente);


module.exports = router;
